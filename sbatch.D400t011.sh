#!/bin/bash
#SBATCH --job-name="D400t011"
#SBATCH --account="project_465000469"
#SBATCH --nodes=32
#SBATCH --time=06:00:00
#SBATCH --ntasks=4096
#SBATCH --ntasks-per-node=128
#SBATCH --mem=128G
#SBATCH --partition=standard
#SBATCH --exclusive
#SBATCH --hint=nomultithread

srun ./qcd1 -i input/qcd1t011.in -c D400t009n4 -noloc -norng
