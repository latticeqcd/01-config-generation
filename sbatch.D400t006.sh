#!/bin/bash
#SBATCH --job-name="D400 test"
#SBATCH --account="project_465000469"
#SBATCH --time=24:00:00
#SBATCH --nodes=32
#SBATCH --ntasks=4096
#SBATCH --ntasks-per-node=128
#SBATCH --mem=128G
#SBATCH --partition=standard
#SBATCH --exclusive
#SBATCH --hint=nomultithread

# does not work, gives: STARTUP_ERROR: Attempt to overwrite old *.log file
#srun ./qcd1 -i input/qcd1t6.in -c D400t006n12 -noloc -noms

srun ./qcd1 -i input/qcd1t6.in -c D400t006n12 -a -noloc -norng
