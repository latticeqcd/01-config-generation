#!/bin/bash
# see https://stackoverflow.com/a/36303809/2768341

run_name="D400t019"
infile="input/qcd1t019.in"
config_dir="/scratch/project_465000469/configs/D400"
project_id="project_465000469"

last_jobid=$(squeue -A "${project_id}" -O "JobID,Name" -h | awk -v rn="$run_name" '$2 ~ rn{print $1}' | sort -r | head -1 | xargs)

if [ -z "${last_jobid}" ]; then
	read_config="$(basename "$(find "${config_dir}" -type f -printf '%T+ %p\n' | sort -r | head -1)")"
	dependency=""
	job_name="${run_name}/$read_config (inital)"
else
	dep="$(squeue -A "${project_id}" --format="%.18i %.50j" -h | sort -k1 -r | awk '{print $3}' | head -1)"
	if [[ "${dep}" =~ "inital" ]]; then
		last_config="${run_name}n0"
	else
		last_config="$(basename $(squeue -A "${project_id}" -O "JobID,Name" -h | sort -k1 -r | awk '{print $2}' | head -1))"
	fi

	read_config="$(echo $last_config | perl -p -e 's/n(\d+)/n.($1+1)/ge')" # increment last config
	dependency="#SBATCH --dependency=afterok:${last_jobid}"
	job_name="${run_name}/$read_config (after:$last_jobid)"
fi

if [ -z "$read_config" ]; then
	echo "No config found in ${config_dir}"
	exit 1
fi

if [[ "${read_config}" =~ "${run_name}" ]]; then
	args="-a"
else
	args="-norng"
fi

sbatch <<EOT
#!/bin/bash
#SBATCH --job-name="$job_name"
#SBATCH --account="project_465000469"
#SBATCH --nodes=64
#SBATCH --time=06:00:00
#SBATCH --ntasks=8192
#SBATCH --ntasks-per-node=128
#SBATCH --mem=224G
#SBATCH --partition=standard
#SBATCH --exclusive
#SBATCH --hint=nomultithread
${dependency}

srun --label --ntasks-per-node 1 free --human | grep -v Swap: | sort

echo "srun ./qcd1 -i ${infile} -c $read_config -noloc ${args}"
srun ./qcd1 -i ${infile} -c $read_config -noloc ${args}

EOT
