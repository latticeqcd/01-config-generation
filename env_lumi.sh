
module load CrayEnv
#module load cray-mpich
module load PrgEnv-gnu # as Isabel suggests

export GCC="gcc" # gnu c compiler
export MPI_HOME="${CRAY_MPICH_PREFIX}"
export MPI_INCLUDE="${MPI_HOME}/include"
