#! python

import numpy as np

kappa_target = np.array([
    0.1344410077,
    0.12784,
    0.1342659145,
    ])

kappa0 = np.array([
    0.13440733,
    0.12784,
    0.13440733,
    ])

def main():
    nstep = 3.0
    print("\n")
    print("Initial kappa:")
    print(kappa0)
    print("\n")
    print("Target kappa:")
    print(kappa_target)
    print("\n")
    for istep in range(int(nstep)):
        kinv = (istep+1)*(1.0/kappa_target - 1.0/kappa0)/nstep + 1.0/kappa0
        print("Step {0}/{1}:".format(istep+1, int(nstep)))
        for ik in range(kappa0.size):
            print("    {0}".format(1.0/kinv[ik]))
        print("\n")


if __name__ == "__main__":
    main()
