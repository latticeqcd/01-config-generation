#!/bin/bash
#SBATCH --job-name="D400t009"
#SBATCH --account="project_465000469"
#SBATCH --nodes=32
#SBATCH --time=24:00:00
#SBATCH --ntasks=4096
#SBATCH --ntasks-per-node=128
#SBATCH --mem=128G
#SBATCH --partition=standard
#SBATCH --exclusive
#SBATCH --hint=nomultithread

srun ./qcd1 -i input/qcd1t9.in -c D400t006n12 -noloc -norng
