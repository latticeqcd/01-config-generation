################################################################################
#
# @see Makefiles in the main dir, call this with make -B to always compile new
#
################################################################################

all: qcd1
.PHONY: qcd1

qcd1:
	rm qcd1 || true
	cd ../main/ && $(MAKE) qcd1
	cp ../main/qcd1 .

clean:
	cd ../main/ && $(MAKE) clean
	rm -f qcd1
