# How to use this repo

Clone the main repo into some directory:

```
git clone https://gitlab.com/rcstar/openQxD-devel.git
```

Cd into the cloned repo and clone this repo:

```
cd openQxD-devel
git clone -b d400 https://gitlab.com/latticeqcd/01-config-generation.git
```

Apply the patch with changes (if needed)

```
git apply 01-config-generation/changes.patch
```

---

To create a patch run in the root dir

```
git diff >01-config-generation/changes.patch
```


# Changelog

* `D400t001` doubled volume from A400
* `D400t002` changed tau->0.5 and N->16
* `D400t003` changed N->12
* `D400t004` changed rational approximations: 12*Ne*delta^2<1e-4 -> speedup even though same number of PF
* `D400t005` fixed ipf indices for strange quark changed tau
* `D400t006` changed tau back to 1.0, added new flavour explicitly
* `D400t007` (RG) switch to lumi, changed to target flavors: "Flavor 0.kappa" = "Deflation subspace generation 0.kappa" = 0.13448496577 (old = 0.13440733) and "Flavour 2.kappa" = 0.13425232699 (old = 0.13440733), RNG.seed = 55722, job_id=lumi:3479378
* `D400t008` (RG) tau->2.0 (old=1.0), nstep->24 (old=12), "solver 5.nmx"=256 (old=128) job_id=lumi:3499237
* `D400t009` (RG) tau->1.0, nstep->12, kappa_ud=0.1344410077, kappa_s=0.1342659145, job_id=3505741
* `D400t010` (RG) tau->1.5, nstep->18, job_id=3519608 (failed)
* `D400t011` (RG) tau->1.0, nstep->12, job_id=3524934 (failed)
* `D400t012` (RG) switch back to original kappas, kappa_ud=0.13440733, kappa_s=0.13440733, ntr->1 job_id=lumi:3530289
* `D400t013` (RG) just continue
* `D400t014` (RG) just continue, crashed
* `D400t015` (RG) just continue, job=3561903,3563158,3572646 (timed out)
* `D400t016` (RG) just continue, job=3581292,3604185
* `D400t017` (RG) kappa_ud=0.1344410077, kappa_s=0.1342659145, kappa_c=0.12784, tau=0.1, nsteps=8, 8k cores, change seed, run name
* `D400t018` (RG) changed process grid from 8x8x8x8 to 8x16x8x8x8, seed incremented
* `D400t019` (RG) changed tau->0.3 (old=0.1), incremented seed, run name, updated run_name="D400t019", infile="input/qcd1t019.in" in sbatch_D400_chain.sh


# Misc

* 07.06.2023 (RG): Jobs started to fail due to OOM. I changed from:
	SBATCH --mem=128G
	to:
	SBATCH --mem=200G

* 29.06.2023 (RG): According to the mail from 26.06.23 from Kurt Lust (LUMI support), I changed:
	SBATCH --mem=200G
	to:
	SBATCH --mem=224G
	to mitigate the OOM problem.


# How to issue chained jobs

If the run name has changed, edit the script sbatch_D400_chain.sh and set the following variables to their new values

```
run_name
infile
```

After that, or if the run name has not changed, just call the script as

```
./sbatch_D400_chain.sh
```

without any argument to issue a new job in the chain. The next config as well as the dependent job are determined automatically.
